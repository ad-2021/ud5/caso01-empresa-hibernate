package app;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import modelo.Articulo;
import modelo.Grupo;

public class MainApp {
	static Session sesion;

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger("org.hibernate");
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - EMPRESA CON HIBERNATE");
		System.out.println("---------------------------");

		try {
			sesion = Conexion.getSession();
			
			// Listar todos los articulos (y su nombre de grupo)
			System.out.println("* Listado de Articulos junto a su nombre de grupo");
			List<Articulo> listadoArt = sesion.createQuery("from Articulo", Articulo.class).list();
			for (Articulo art : listadoArt) {
				System.out.println(art.getNombre() + " - " + art.getPrecio() + " - " + art.getGrupo().getDescripcion());
			}
			
			// Ejemplo de inserción
			System.out.println("* Inserción de un nuevo grupo");
			Grupo grupo = new Grupo("Consumibles");
			sesion.beginTransaction();
			sesion.save(grupo);
			sesion.getTransaction().commit();
			System.out.println("¡Insertado grupo!");

			// Ejemplo de actualización
			System.out.println("* Modificación de un grupo");
			Grupo g = sesion.get(Grupo.class, 1);
			g.setDescripcion("HW");
			sesion.beginTransaction();
			sesion.update(g);
			sesion.getTransaction().commit();
			System.out.println("¡Modificado grupo!");

			// Ejemplo de borrado
			System.out.println("* Borrado de un grupo");
			sesion.beginTransaction();
			sesion.delete(grupo);
			sesion.getTransaction().commit();
			System.out.println("¡Borrado grupo!");

			// Recorrer los articulos asociados a un grupo concreto (no es necesario
			// el acceso explícito a la bd)
			System.out.println("* Articulos del grupo: " + grupo.getDescripcion());
			Set<Articulo> articulos = g.getArticulos();
			for (Articulo art : articulos) {
				System.out.println(art.getNombre() + " - " + art.getPrecio());
			}

			// Obtener solo nombres de articulos
			System.out.println("* Listado de los nombres de los articulos");
			List<String> listadoN = sesion.createQuery("select nombre from Articulo", String.class).list();
			listadoN.forEach(System.out::println);

			// Obtener solo id y precio de articulos
			System.out.println("* Listado de los id + precio de los articulos");
			List<Object[]> listadoP = sesion.createQuery("select id, precio from Articulo", Object[].class).list();
			listadoP.forEach((reg) -> {
				System.out.println("ID: " + reg[0] + " - Precio: " + reg[1]);
			});

			Conexion.closeSession();

			
		} catch (Exception e) {
			System.err.println("Error en alguna operación del programa..." + e.getMessage());
		}
		
		System.out.println("-------------------------------");
		System.out.println("FIN APP - EMPRESA CON HIBERNATE");

	}

}
